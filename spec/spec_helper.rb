# frozen_string_literal: true

require 'httparty'
require 'jwt'
require 'click_house'
require 'csv'

def cube_security_headers
  payload = {
    iat: Time.now.utc.to_i,
    exp: Time.now.utc.to_i + 180,
    appId: 'gitlab_project_20001' # change this to a test one later
  }

  {
    "Content-Type": 'application/json',
    Authorization: JWT.encode(payload, '687rewhjfgiu2u389gujoigdoghjr901qft', 'HS256')
  }
end

def cube_query(query)
  HTTParty.post("http://#{ENV['CI'] ? 'cube' : '0.0.0.0'}:4000/cubejs-api/v1/load",
                body: { query: query }.to_json,
                headers: cube_security_headers)
end

def get_cubes
  HTTParty.get("http://#{ENV['CI'] ? 'cube' : '0.0.0.0'}:4000/cubejs-api/v1/meta",
                headers: cube_security_headers)
end

RSpec.configure do |config|
  # rspec-expectations config goes here. You can use an alternate
  # assertion/expectation library such as wrong or the stdlib/minitest
  # assertions if you prefer.
  config.expect_with :rspec do |expectations|
    # This option will default to `true` in RSpec 4. It makes the `description`
    # and `failure_message` of custom matchers include text for helper methods
    # defined using `chain`, e.g.:
    #     be_bigger_than(2).and_smaller_than(4).description
    #     # => "be bigger than 2 and smaller than 4"
    # ...rather than:
    #     # => "be bigger than 2"
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  # rspec-mocks config goes here. You can use an alternate test double
  # library (such as bogus or mocha) by changing the `mock_with` option here.
  config.mock_with :rspec do |mocks|
    # Prevents you from mocking or stubbing a method that does not exist on
    # a real object. This is generally recommended, and will default to
    # `true` in RSpec 4.
    mocks.verify_partial_doubles = true
  end

  # This option will default to `:apply_to_host_groups` in RSpec 4 (and will
  # have no way to turn it off -- the option exists only for backwards
  # compatibility in RSpec 3). It causes shared context metadata to be
  # inherited by the metadata hash of host groups and examples, rather than
  # triggering implicit auto-inclusion in groups with matching metadata.
  config.shared_context_metadata_behavior = :apply_to_host_groups

  # The settings below are suggested to provide a good initial experience
  # with RSpec, but feel free to customize to your heart's content.
  #   # This allows you to limit a spec run to individual examples or groups
  #   # you care about by tagging them with `:focus` metadata. When nothing
  #   # is tagged with `:focus`, all examples get run. RSpec also provides
  #   # aliases for `it`, `describe`, and `context` that include `:focus`
  #   # metadata: `fit`, `fdescribe` and `fcontext`, respectively.
  #   config.filter_run_when_matching :focus
  #
  #   # Allows RSpec to persist some state between runs in order to support
  #   # the `--only-failures` and `--next-failure` CLI options. We recommend
  #   # you configure your source control system to ignore this file.
  #   config.example_status_persistence_file_path = "spec/examples.txt"
  #
  #   # Limits the available syntax to the non-monkey patched syntax that is
  #   # recommended. For more details, see:
  #   #   - http://rspec.info/blog/2012/06/rspecs-new-expectation-syntax/
  #   #   - http://www.teaisaweso.me/blog/2013/05/27/rspecs-new-message-expectation-syntax/
  #   #   - http://rspec.info/blog/2014/05/notable-changes-in-rspec-3/#zero-monkey-patching-mode
  #   config.disable_monkey_patching!
  #
  #   # This setting enables warnings. It's recommended, but in some cases may
  #   # be too noisy due to issues in dependencies.
  #   config.warnings = true
  #
  #   # Many RSpec users commonly either run the entire suite or an individual
  #   # file, and it's useful to allow more verbose output when running an
  #   # individual spec file.
  #   if config.files_to_run.one?
  #     # Use the documentation formatter for detailed output,
  #     # unless a formatter has already been configured
  #     # (e.g. via a command-line flag).
  #     config.default_formatter = "doc"
  #   end
  #
  #   # Print the 10 slowest examples and example groups at the
  #   # end of the spec run, to help surface which specs are running
  #   # particularly slow.
  #   config.profile_examples = 10
  #
  #   # Run specs in random order to surface order dependencies. If you find an
  #   # order dependency and want to debug it, you can fix the order by providing
  #   # the seed, which is printed after each run.
  #   #     --seed 1234
  #   config.order = :random
  #
  #   # Seed global randomization in this process using the `--seed` CLI option.
  #   # Setting this allows you to use `--seed` to deterministically reproduce
  #   # test failures related to randomization by passing the same `--seed` value
  #   # as the one that triggered the failure.
  #   Kernel.srand config.seed

  config.before(:all) do
    puts '🌱 Creating and seeding ClickHouse database'
    ClickHouse.connection.create_database('gitlab_project_20001', if_not_exists: true, engine: nil, cluster: nil)

    ClickHouse.connection.execute <<~SQL
        create table if not exists gitlab_project_20001.snowplow_events
      (
          app_id  String,
          platform String,
          etl_tstamp Nullable(DateTime64(3, 'UTC')),
          collector_tstamp DateTime64(3, 'UTC'),
          dvce_created_tstamp Nullable(DateTime64(3, 'UTC')),
          event String,#{' '}
          event_id String,#{' '}
          txn_id Nullable(String),#{' '}
          name_tracker Nullable(String),
          v_tracker String,#{' '}
          v_collector String,
          v_etl String,#{' '}
          user_id Nullable(String),
          user_ipaddress Nullable(String),
          user_fingerprint Nullable(String),#{' '}
          domain_userid Nullable(String),#{' '}
          domain_sessionidx Nullable(String),
          network_userid Nullable(String),
          geo_country Nullable(String),
          geo_region Nullable(String),
          geo_city Nullable(String),
          geo_zipcode Nullable(String),
          geo_latitude Nullable(String),
          geo_longitude Nullable(String),
          geo_region_name Nullable(String),
          ip_isp Nullable(String),
          ip_organization Nullable(String),
          ip_domain Nullable(String),
          ip_netspeed Nullable(String),
          page_url Nullable(String),
          page_title Nullable(String),
          page_referrer Nullable(String),
          page_urlscheme Nullable(String),
          page_urlhost Nullable(String),
          page_urlport Nullable(String),
          page_urlpath Nullable(String),
          page_urlquery Nullable(String),
          page_urlfragment Nullable(String),
          refr_urlscheme Nullable(String),
          refr_urlhost Nullable(String),
          refr_urlport Nullable(String),
          refr_urlpath Nullable(String),
          refr_urlquery Nullable(String),
          refr_urlfragment Nullable(String),
          refr_medium Nullable(String),
          refr_source Nullable(String),
          refr_term Nullable(String),
          mkt_medium Nullable(String),
          mkt_source Nullable(String),
          mkt_term Nullable(String),
          mkt_content Nullable(String),
          mkt_campaign Nullable(String),
          contexts String,
          unstruct_event String,
          pp_xoffset_min Nullable(String),
          pp_xoffset_max Nullable(String),
          pp_yoffset_min Nullable(String),
          pp_yoffset_max Nullable(String),
          useragent Nullable(String),
          br_name Nullable(String),
          br_family Nullable(String),
          br_version Nullable(String),
          br_type Nullable(String),
          br_renderengine Nullable(String),
          br_lang Nullable(String),
          br_features_pdf Nullable(String),
          br_cookies Nullable(String),
          br_colordepth Nullable(Int16),
          br_viewwidth Nullable(Int16),
          br_viewheight Nullable(Int16),
          os_name Nullable(String),
          os_family Nullable(String),
          os_manufacturer Nullable(String),
          os_timezone Nullable(String),
          dvce_type Nullable(String),
          dvce_ismobile Nullable(String),
          dvce_screenwidth Nullable(Int16),
          dvce_screenheight Nullable(Int16),
          doc_charset Nullable(String),
          doc_width Nullable(String),
          doc_height Nullable(String),
          geo_timezone Nullable(String),
          mkt_clickid Nullable(String),
          mkt_network Nullable(String),
          etl_tags Nullable(String),
          dvce_sent_tstamp Nullable(DateTime64(3, 'UTC')),
          refr_domain_userid Nullable(String),
          refr_device_tstamp Nullable(String),
          derived_contexts Nullable(String),
          domain_sessionid Nullable(String),
          derived_tstamp   DateTime64(3, 'UTC'),
          event_vendor Nullable(String),
          event_name Nullable(String),
          event_format Nullable(String),
          event_version Nullable(String),
          event_fingerprint Nullable(String),
          true_tstamp Nullable(String),
          custom_event_name Nullable(String),
          custom_event_props Nullable(String),
          custom_user_props Nullable(String),
          user_id_type Enum8('anonymous' = 1, 'cookie' = 2, 'identify' = 3)
      )
          ENGINE = MergeTree#{' '}
            ORDER BY collector_tstamp
            SETTINGS index_granularity = 8192
    SQL
    ClickHouse.connection.execute(File.open('spec/fixtures/snowplow_events.sql').read)
  end

  config.after(:all) do
    ClickHouse.connection.execute('DROP TABLE IF EXISTS gitlab_project_20001.snowplow_events')
    puts "\n\n🔥 Database torn down"
  end
end

ClickHouse.config do |config|
  config.adapter = :net_http
  config.database = 'gitlab_project_20001'
  config.url = "http://#{ENV['CI'] ? 'clickhouse' : '0.0.0.0'}:8123"
  config.timeout = 60
  config.open_timeout = 3
  config.ssl_verify = false
  config.symbolize_keys = false
  config.headers = {}
  config.username = 'test'
  config.password = 'test'
  config.json_parser = ClickHouse::Middleware::ParseJson
  config.json_serializer = ClickHouse::Serializer::JsonSerializer
end
