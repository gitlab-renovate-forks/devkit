# Product Analytics DevKit

A quick and easy way to get up and running with a local environment of Snowplow +
Clickhouse + Cube

```mermaid
graph LR
    A[User visits page] --> |JS tracker| B[Snowplow - Port 9091]
    B --> |Streams tracked info| C[ClickHouse - Port 8123]
    C --> |Data available| D[Cube - Port 4000]
    D --> |Exportable graphs/API data| E[GitLab UI]
    D --> F[GitLab API?]
```

## Prerequisites

* Docker
   * Note that some Docker replacements such as podman are not supported at this time.
   * For Rancher Desktop to work, set **Virtual Machine Type** to [**VZ**](https://docs.rancherdesktop.io/ui/preferences/virtual-machine/emulation#vz) and **Mount Type** to [**virtiofs**](https://docs.rancherdesktop.io/ui/preferences/virtual-machine/volumes/#virtiofs).
   * GitLab team members require a [Pro license](https://www.docker.com/pricing/) to use Docker for Mac. This can be acquired by following [the process in the handbook](https://handbook.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop).

## Quickstart

1. Use [Gitpod](#connecting-an-environment-to-gitpod).

Alternatively:

1. Clone this repo.
1. Copy the cube configuration file, altering any values that need to be changed. `cp cube/.env.sample cube/.env`.
   * Local use within docker will require no changes.
   * If your GDK GitLab host is not `localhost`, modify `GITLAB_BASE_URI` to point to your GitLab instance (i.e. `http://gdk.test:3000`)
1. Run `docker compose up`
1. Connect the [DevKit to the GitLab GDK](#connecting-gdk-to-your-devkit).

## Useful links/info

- Clickhouse: `http://localhost:8123/play`
- Cube: `http://localhost:4000`

### Ad Blockers / Browser Extensions

Be mindful of running ad blockers when working with our instrumentation libraries, whether they're on localhost, GitLab.com, or anywhere else. Ad blocking extension _will_ block Snowplow from working which will prevent events from being sent to our collectors.

Furthermore, our JS SDK may fail to run if you have certain browser extensions, such as DuckDuckGo, Privacy Badger, uBlock Origin, etc. enabled on your GDK or localhost. Extensions like these will automatically enable Global Privacy Control (GPC) which may cause the SDK to malfunction or fail entirely.

## Test credentials

- Service: `username // password`
- Clickhouse: `test // test`, defined in `clickhouse/users.xml`.
- Cube API key: `thisisnotarealkey43ff15165ce01e4ff47d75092e3b25b2c0b20dc27f6cd5a8aed7b7bd855df88c9e0748d7afd37adda6d981c16177b086acf496bbdc62dbb`, defined in `cube/.env`, sample available at `cube/.env.sample`
- Analytics Configurator: `test // test`, defined in `docker-compose.yml`.

## Connecting GDK to your devkit

The following commands should be run in your GitLab directory.

1. Follow the [instructions](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/product_analytics.md#setup) to set up Product Analytics on your GDK instance if you haven't already done so.
1. Run the auto configuration script pointing to your GDK Gitlab installation directory
   - i.e. from this devkit root folder run `yarn configure-gitlab PATH/TO/GDK/GITLAB`
   - Requires either `asdf` to be installed or `node` and `yarn` with the versions `.tool-versions` to be installed (this should be installed if GDK is installed)
1. Go to your test project, e.g., Gitlab Org > Gitlab Shell, and view the Dashboard listing via this URL:
   `{GDK_HOST}/gitlab-org/gitlab-shell/-/analytics/dashboards`
1. The onboarding flow should prompt you to set up product analytics, do so by
   clicking the "Set up product analytics" button.
1. Assuming everything works as intended, you'll be prompted with instructions
   to set up instrumentation.

## Connecting an environment to Gitpod

1. Visit the environment or [GDK Gitpod workspace](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/gitpod.md) you want to setup (e.g https://gitlab.com) and enable the feature flags detailed in the [Product Analytics documentation](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/product_analytics.md#feature-flags).
    * _Note: Product Analytics **requires** an Ultimate license._
2. Copy the GitLab URL from the above environment/workspace and save it somewhere for later.
1. Click the **Gitpod** button in the DevKit repository. This might require you to enable the Gitpod integration
in your user settings.
    * _Hint: If you don't see a "Gitpod" button, open the dropdown of the "Web IDE" split button._
    * If you have never used Gitpod before, you must:
        * Create a new Gitpod account.
        * Connect the Gitpod account to your GitLab account.
1. After the DevKit Gitpod workspace has opened:
    1. Enter the GitLab URL from `.2` when prompted.
    1. Enter the [GitLab container registry](#logging-into-the-gitlab-container-registry) password into the Gitpod terminal when prompted.
1. Verify that the Cube instance is running by visiting the Gitpod URL for port `4000` (under the `PORTS` tab).
    * _Note: If you want to use the Cube playground you will need to [set the security context](#cube)._
1. Visit the GitLab URL from `.2`.
1. If this is a fresh install, go to **Admin Area > Settings > General > Product analytics**:
    1. Click **Enable product analytics**.
    1. Copy the DevKit Gitpod workspaces URL for port `4567` (under the `PORTS` tab) and enter it into the **Snowplow configurator connection string** field with [analytics configurator credentials](#test-credentials).
        * _Hint: As an example - `http://test:test@https://gitlaborganalyti-devkit-xxxx.gitpod.io:4567`_.
    1. Copy the DevKit Gitpod workspaces URL for port `9091` (under the `PORTS` tab) and enter it into the **Collector host** field.
    1. Copy the DevKit Gitpod workspaces URL for port `4000` (under the `PORTS` tab) and enter it into the **Cube API URL** field.
    1. Copy the Cube API key from [test credentials](#test-credentials) and enter it into the **Cube API key** field.
    1. Click **Save changes**.
1. Create or go to a project within GitLab.
1. If this is an existing install and you want to override the existing instance-level settings, within the project, go to **Settings > Analytics > Data sources** and fill the same details as the admin area in `7.`.
1. Within the project, go to **Analyze > Analytics dashboards**.
1. Click **Set up** next to the **Product Analytics** item.
1. Click **Set up product analytics** and wait for the set up process to complete.
   1. Select an [SDK](https://docs.gitlab.com/ee/user/product_analytics/#instrument-your-application) and follow the instrumentation steps to send data.
       * For the JS SDK you can also manually send cURL events instead:
         ```bash
           curl '[URL_FROM_STEP_7.3]/com.snowplowanalytics.snowplow/tp2' \
           -H 'content-type: application/json; charset=UTF-8' \
           --data-raw '{"schema":"iglu:com.snowplowanalytics.snowplow/payload_data/jsonschema/1-0-4","data":[{"e":"ue","eid":"804e7e2e-eff5-4ee3-928c-4a74d1e9163e","tv":"js-3.12.0","tna":"gitlab","aid":"6utUsNWHHPSaGAO5vfuXpw","p":"web","cookie":"1","cs":"UTF-8","lang":"en-GB","res":"3840x1080","cd":"24","dtm":"1693437468146","vp":"1265x934","ds":"1265x934","url":"http://localhost:8080/","stm":"1693437468149"}]}' \
           --compressed
         ```
1. Once the events have been processed, you will be shown the Product Analytics dashboards automatically.

### Generate events from GDK

1. Make sure CORS is disabled on your GDK.
   - Follow these instructions to [disable CORS](#disable-cors).
1. Install this tracking userscript https://gitlab.com/-/snippets/2435170.
   - Note: We recommend you use a userscript management extension like [Tampermonkey](https://www.tampermonkey.net/).
   - Note: By default this script will send events to our sandbox.
1. Edit this script to send events to your local devkit.
   - Edit the `host` to be your Snowplow URL, default `http://localhost:9091`.
   - Edit the `appId` to be your Snowplow application id.
   - If your hostname isn't `localhost`, add a `@match` directive for your hostname in the comments
      - i.e. `// @match       *://gdk.test:3000/*`
1. Disable `Do Not Track` and `Global Privacy Control` in your browser's settings.
   - **Firefox:** Visit `about:config` and search for `globalprivacycontrol` to disable it.
1. To generate events simply browse GDK or gitlab.com with the script enabled.
   - Note: As soon as the first event is captured a new Clickhouse database will be
     be created the tracked project. You can confirm this by running `SHOW databases`
     in Clickhouse. You should see a new database called `gitlab_project_[GITLAB_PROJECT_ID]` as
     soon an an event has been captured.
1. You can now use Cube to view events by creating queries.
   - Note: By default Cube will use the wrong database. You need to modify
     the `appId` to match your project store, eg `gitlab_project_3`.
     See the [cube](#cube) section below for additional instructions.

## Cube

Cube uses security contexts to scope data by project. When running queries in the cube playground, or via the API,
make sure to use the security context by clicking on `Edit Security Context` (or `Add Security Context` for first time setup) button and entering the following:

```json
{
  "iat": UNIX timestamp after now,
  "exp": UNIX timestamp in the future,
  "appId": Name of the database to query. "gitlab_project_199" in the example given above.
}
```

**Gotcha:** If the UNIX timestamps are incorrect, or expired, you'll likely see errors about `appId` not being defined.
See [troubleshooting](#cube-security-contexts-jwt-results-in-appid-undefined-error).

### Developing Cube.js

If you need to make changes to Cube.js, we have a [guide](./docs/developing-cubejs.md) to help you get set up.

### Schema Development

As part of a group discussion on where Cube schemas should be developed, they should start in this repo (`devkit`), and, once tested, be graduated for production use at [`analytics-stack`](https://gitlab.com/gitlab-org/analytics-section/product-analytics/analytics-stack/), our production-intended version of the Analytics stack.

#### Testing

To test your schema changes, you'll need to add specs. Currently we're using rspec in `spec/schema` to test the schemas.

Run the specs by running `gitlab-runner exec docker --docker-privileged schema_specs`. This will create a testing environment within a Docker container, and run the specs.

## Troubleshooting

### Disable CORS

Events will be blocked unless CORS is disabled on GDK.

In `config/gitlab.yml`, disable `content-security-policy` so that events can
be emitted to the collector:
   ```yaml
      development:
         gitlab:
          allowed_hosts: ['localhost', 'gdk.test']
          content_security_policy:
            enabled: false
            directives:
              connect_src: "'self' http://gdk.test:* http://localhost:* http://127.0.0.1:* ws://gdk.test:* ws://localhost:* wss://gdk.test:* wss://localhost:* ws://127.0.0.1:* https://snowplow.trx.gitlab.net/"
   ```

Also ensure any tracking/ad blockers are disabled on your GDK, otherwise it will
not be able to send events. Also ensure `Do Not Track` and `Global Privacy Control` browser settings are disabled.

### Cube security contexts JWT results in appId undefined error

Cube's default token value contains UNIX timestamps for [`iat`](https://www.rfc-editor.org/rfc/rfc7519#section-4.1.6) and [`exp`](https://www.rfc-editor.org/rfc/rfc7519#section-4.1.4).
If you are setting the `appId` property in the security context but it isn't being used then it will be because these timestamps have expired.

This will show up as the error `TypeError: Cannot read property 'appId' of undefined`.

To fix this problem, [regenerate](https://www.epochconverter.com/) the UNIX timestamps (seconds since the UNIX epoch). Make sure that the `iat` is the current timestamp and set `exp` to be a timestamp sometime in the future.


## Snowplow

### Project setup

You can set up a project to track events using GDK or by manually creating a CURL request.

#### Using GDK

1. Complete the [connecting GDK to your devkit](#connecting-gdk-to-your-devkit) steps for Snowplow.
1. Select a project that has not been set up with Jitsu or create a new project.
1. Visit **Project > Analytics > Dashboards** and click on the `Set up` button to start the onboarding process.

#### Manually setting up a project

Run `curl -X POST http://localhost:4567/setup-project/[project_name] -u test:test` to set up a project. This will create a separate database for the project called `[project_name]_db` and return an `app_id` that needs to be used with any SDK to send data to this database.

### Getting events

See [developing with the devkit section](https://gitlab.com/gitlab-org/analytics-section/product-analytics/gl-application-sdk-js/-/blob/main/packages/js-sdk/README.md#developing-with-the-devkit) in the SDK's readme to send events using an example application.
