## End-to-end(e2e) Tests
### Summary
End-to-end tests are setup to run for Product Analytics devkit & [gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab) MRs.

#### e2e tests running for devkit

The tests are executed for every devkit MR. There are two e2e tests related jobs in the devkit pipeline:

1. `build_analytics_gdk` - Build custom GDK docker image to include necessary Product Analytics setup. It's based on GDK docker image built in **gitlab-org/gitlab** pipelines using [gitlab-qa-gdk Dockerfile](https://gitlab.com/gitlab-org/gitlab/-/blob/master/qa/gdk/Dockerfile.gdk).
1. `e2e_tests` - GDK and Product Analytics services are started via docker-compose based on `docker-compose.e2e.yml` and the tests are executed against that setup using `gitlab-ee-qa` docker image. `gitlab-ee-qa` docker image is build within **gitlab-org/gitlab** pipelines using [gitlab-ee-qa Dockerfile](https://gitlab.com/gitlab-org/gitlab/-/blob/master/qa/Dockerfile).

#### e2e tests running for gitalb-org/gitlab
- The job to trigger product analytics tests is defined in `qa.gitlab-ci.yml` - [e2e:test-product-analytics](https://gitlab.com/gitlab-org/gitlab/-/blob/2d9028ab4a135a4b08d9aa7c1aac23f784930fd8/.gitlab/ci/qa.gitlab-ci.yml#L232).
- The tests are executed based on pipeline rules [.qa:rules:e2e:test-on-gdk](https://gitlab.com/gitlab-org/gitlab/-/blob/2d9028ab4a135a4b08d9aa7c1aac23f784930fd8/.gitlab/ci/rules.gitlab-ci.yml#L1754). 
- In order to execute product analytics tests for a specific MR in **gitlab-org/gitlab** we are triggering a pipeline on [devkit mirror](https://gitlab.com/gitlab-org/analytics-section/product-analytics/product-analytics-devkit-mirror) as [multi-project downstream pipeline](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#multi-project-pipelines) and are passing `GDK_IMAGE` & `GITLAB_QA_IMAGE` variables. Those variables hold values for docker images that were built based on a specific **gitlab-org/gitlab** MR in that MR's pipeline.
- Once the tests are executed the results can be found in `e2e_tests` job of devkit's mirror pipeline that was triggered as explained above.
- We are using devkit's mirror and not devkit repository for the pipelines due to a limitation on devkit where only `maintainer` role can trigger pipelines on devkit against protected branches. More details on this can be found in the related [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/480462), specifically this [comment](https://gitlab.com/gitlab-org/gitlab/-/issues/480462#note_2073065414).

#### Notes
In order to help debugging issues with the tests we have job's artifacts containing `docker-compose.log` and screenshots for test failures.

### Keeping GDK docker image up to date for devkit
`gitlab-qa-gdk` docker image is pinned to a specific manifest digest [here](https://gitlab.com/gitlab-org/analytics-section/product-analytics/devkit/-/blob/main/docker_images/e2e_gdk/Dockerfile?ref_type=heads#L1). This digest has to be updated at least weekly to pick up new Gitlab changes and because older digests get cleaned up by docker registry garbage collection. If digest that is pinned in devkit is cleaned up the `build_analytics_gdk` job will start failing as it won't be able to pull the image with specified digest.

**Steps to update the digest:**
1. Find latest digest for `gitlab-qa-gdk` image `master` tag in Gitlab container registry at [gitlab/gitlab-qa-gdk](https://gitlab.com/gitlab-org/gitlab/container_registry/4039255) path. After following the link search for "master", expand `master` tag section and copy manifest digest. 
3. Update the docker image digest in devkit. See [example MR](https://gitlab.com/gitlab-org/analytics-section/product-analytics/devkit/-/merge_requests/141).

### How to set up and validate locally

It is possible to run the same test that runs in CI locally by following the steps below.

**Notes:**

* The best is to use separate clone of the devkit repo as the test requires empty clickhouse datastore. To remove clickhouse data on your devkit repo clone you can run `rm -rf clickhouse/data` from devkit root directory, however there might be some other changes on your local devkit, hence using separate repo clone is still preferable.
* Assumption is that devkit setup and `docker login` for gitlab registry are done as per [README](https://gitlab.com/gitlab-org/analytics-section/product-analytics/devkit/-/blob/main/README.md?ref_type=heads#quickstart).
* When running on Apple Silicon(M1, M2, M3) enable this setting in Docker general settings -\> `Use Rosetta for x86/amd64 emulation on Apple Silicon`

**Steps:**

* Stop GDK
  ```
  # From Gitlab Development kit
  gdk stop
  ```
* Stop devkit if it's up and running
  ```
  # From devkit root directory
  docker-compose down
  ```

* Replace `GITLAB_BASE_URI`  in `cube/.env`  to be `http://gdk.test:3000`
* Build custom GDK image

  ```
  docker build --no-cache -t custom_gdk ./docker_images/e2e_gdk
  ```
* Add `EE_LICENSE` env variable

  ```
  # Use path to you valid ultimate license file
  export EE_LICENSE=$(cat /Users/some-user/Code/ultimate-100.gitlab-license)
  ```
* Add `QA_ADMIN_ACCESS_TOKEN` env variable. (Use token that is seeded into gdk image [here](https://gitlab.com/gitlab-org/gitlab/-/blob/master/db/fixtures/development/25_api_personal_access_token.rb?ref_type=heads#L4)).

  ```
  export QA_ADMIN_ACCESS_TOKEN=<token>
  ```
* Replace gdk.test image in docker-compose.e2e.yml to use local image `custom_gdk`
* Run the tests with the following command from devkit root directory

  ```
  docker-compose -f docker-compose.e2e.yml run gitlab-qa
  ```
* Remove docker containers that were used for the tests
  ```
  docker-compose -f docker-compose.e2e.yml down
  ```
