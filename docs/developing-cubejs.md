# Developing Cube.js

[Cube.js](https://cube.dev/) is the system the Frontend uses to query for data and return structured results.
The Clickhouse integration we use is community-supported. So there may be times where we will want to upstream new
features or bugfixes to this integration or other areas of Cube.js' codebase.

This guide aims to help you set up a local Cube.js repository and connect it to your local DevKit containers to be able to test
changes using your local [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit).

> Note: These steps are based on the steps outlined in [Cube.js' contribution guide](https://github.com/cube-js/cube/blob/master/CONTRIBUTING.md):

## Prerequisites

* [DevKit](../README.md) running locally.
* The tools from [`.tool-versions`](../.tool-versions) installed.
    * `npx` is included in `node` ever since `v8.2.0`.
* `docker` running for integration tests, because they use the [testcontainers](https://www.npmjs.com/package/testcontainers)
  NPM package to spin up isolated containers for each integration test.

## Set up

### Your local Cube.js

1. [Fork](https://docs.github.com/en/get-started/quickstart/fork-a-repo) the Cube.js repository at https://github.com/cube-js/cube.
1. Optional. If you are testing a branch or patch of the Cube.js repository, switch to the branch or apply the patch now.
1. In your terminal, change directory to your Cube.js repository.
1. Run `yarn install`. 
   * Note: Java errors are okay and expected. This is because the Java package is not currently supported on
     Apple Silicon (M1/M2). Unless you are working on one of the drivers requiring a Java connection, this won't affect
     your ability to run Cube.js.
   * Note: Gyp errors are [related](https://stackoverflow.com/questions/74715990/node-gyp-err-invalid-mode-ru-while-trying-to-load-binding-gyp)
     to `python 3.11` which you can choose to fix. However, it isn't required for the Clickhouse driver to function. So it can be ignored.
1. Run `yarn link:dev` to link the various packages.
1. Run `cd packages/cubejs-schema-compiler && yarn install; cd ../../` to install dependencies for drivers and dependent packages.
1. Run `cd packages/cubejs-server-core && yarn link @cubejs-backend/schema-compiler && cd ../../` to link the schema compiler for the clickhouse driver.
1. Run `yarn build` to build the frontend dependent packages.
1. Run `cd packages/cubejs-playground && yarn build && cd ../../` to build the playground frontend.
1. Run `yarn tsc:watch` to start the TypeScript compiler in watch mode.

### Your test project

1. [Using a separate terminal tab/window]: Go to the directory you like to store your projects, .e.g `cd ~/www`
1. Run `npx cubejs-cli create test-cubejs -d clickhouse` to use the [Cube.js CLI](https://cube.dev/docs/product/workspace/cli#quickstart) create a test project.
1. Run `cd test-cubejs` to enter your test projects directory.
1. Run `grep '@cubejs-backend/server' ./package.json` to see what version of `@cubejs-backend/server` it expects.
1. Run `grep 'version' <CUBE_DIRECTORY>/packages/cubejs-server/package.json` to see what version your Cube.js repository is using.
1. Optional. If the Cube.js versions are different, update the `@cubejs-backend/server` version in your test project's `./package.json` to match the Cube.js repository version.
1. Run `yarn install` to make sure your `yarn.lock` is up-to-date.
1. From the DevKit's [`cube/` folder](../cube), copy and store in the root directory of your test project:
    * `cube.js`
    * `.env.sample`
    * `schema/`
    * `utils/`
1. Run `cp .env.sample .env` to rename the sample file and overwrite the provided `.env` file.
1. Using your editor, update the `.env` file:
    1. Change the `GITLAB_BASE_URI` value from `http://host.docker.internal:3000` to your usual GDK URL (for example, `http://localhost:3000`).
    1. Change the `CUBEJS_DB_HOST` value from `clickhouse` to `localhost`.
1. Run `yarn link @cubejs-backend/server-core` to link your local Cube.js repositories package to the test project.
1. Copy the DevKit's [security context](../README.md#cube) from your running [Cube.js playground](http://localhost:4000/) if you have it set up and save it somewhere for later.
1. If running, stop the DevKit's Cube.js server instance as the local Cube.js repository uses the same port.
1. Run `yarn dev` and visit: `http://localhost:4000/`.
1. Update the [security context](../README.md#cube) using the details from `12.`, or create a new security context. 

## Making changes to the Cube.js repository

Each package is self-contained with its own `package.json`, `node_modules`, and `tests`.

When making changes to the Cube.js repository, as long as `yarn tsc:watch` is running at the root of the Cube.js repository, your local version will automatically update upon a page refresh.
If this doesn't happen, it might be because you're making changes to a package that hasn't been linked either in the Cube.js repository or your test project.
Each location where the package is used internally in the Cube.js repository must have had `yarn link [THE_PACKAGE]` applied to it.
As long as the package is used by `@cubejs-backend/server-core`, it will be picked up by your test project.

After making your changes, run `yarn lint` in the package that was changed within the Cube.js repository.
This doesn't happen automatically unless you've set up your code editor to do so.

## Running unit tests

1. In the package being edited, for instance `packages/cubejs-schema-compiler`, the tests should be written in typescript under `test/unit`. You can run:
    * `yarn unit` to run all the unit tests for that particular package.
    * `jest` to test an individual unit test: `TZ=UTC yarn jest dist/test/unit/clickhouse-query.test.js --watch`
        * Note: Individual tests are run against the compiled JS file _not_ the TS file.

## Running integration tests

1. Within the package being edited, for instance `packages/cubejs-schema-compiler`, the tests should be written in TypeScript under `test/integration`. You can run:
    * `yarn integration` to run all the integration tests for that particular package.
    * `jest` to test an individual integration tests: `TZ=UTC yarn jest dist/test/integration/clickhouse/clickhouse-dataschema-compiler.test.js --watch`
        * Note: Individual tests are run against the compiled JS file _not_ the TS file.

## Troubleshooting

### Fetch error caused by funnels schema

The funnels schema is not protected from fetch-related errors. If you're seeing a fetch error while using
your Cube.js instance, it is recommended to remove the offending code for now. Funnels are not in active development,
and there is an [issue](https://gitlab.com/gitlab-org/analytics-section/product-analytics/devkit/-/issues/24)
to improve the implementation to be more resistant to errors.

To remove the offending code:

1. Go to your test projects directory.
1. Run `rm -f schema/Funnels.js` to delete the `Funnels.js` file.

This means that you won't be able to test the
[funnel analysis](https://docs.gitlab.com/ee/user/product_analytics/#funnel-analysis) API endpoint.

### Dist directory doesn't exist

When installing using Yarn, if you get an error related to a missing `dist` directory:

```bash
Cannot find module '/Users/roberthunt/www/cube/node_modules/@cubejs-backend/shared/dist/src/index.js'. Please verify that the package.json has a valid "main" entry
```

1. Go to the appropriate `package/` directory.
1. Run `yarn build`.
1. Re-run `yarn install` where you were running it before.
      
### Typescript files not updating

Cube.js uses TypeScript configured in incremental mode, which uses cache to speed up compilation.
In some cases, you can run into a problem where this process is not recompiling a file.
To fix it, we recommend running `yarn clean` and `yarn tsc --watch`.

### Integration tests not running

If you receive an error such as `(HTTP code 409) container stopped/paused - Container 123xxx is not running` while
trying to run the integration tests, it is because the `yandex/clickhouse-server` NPM package does not support M1 macs.
To fix this, update the package to `clickhouse/clickhouse` instead:

```patch
Index: packages/cubejs-clickhouse-driver/test/ClickHouseDriver.test.ts
===================================================================
diff --git a/packages/cubejs-clickhouse-driver/test/ClickHouseDriver.test.ts b/packages/cubejs-clickhouse-driver/test/ClickHouseDriver.test.ts
--- a/packages/cubejs-clickhouse-driver/test/ClickHouseDriver.test.ts	(revision Staged)
+++ b/packages/cubejs-clickhouse-driver/test/ClickHouseDriver.test.ts	(date 1692805382902)
@@ -24,7 +24,7 @@
 
     const version = process.env.TEST_CLICKHOUSE_VERSION || 'latest';
 
-    container = await new GenericContainer(`yandex/clickhouse-server:${version}`)
+    container = await new GenericContainer(`clickhouse/clickhouse-server:latest`)
       .withExposedPorts(8123)
       .start();
 
Index: packages/cubejs-schema-compiler/test/integration/clickhouse/ClickHouseDbRunner.js
===================================================================
diff --git a/packages/cubejs-schema-compiler/test/integration/clickhouse/ClickHouseDbRunner.js b/packages/cubejs-schema-compiler/test/integration/clickhouse/ClickHouseDbRunner.js
--- a/packages/cubejs-schema-compiler/test/integration/clickhouse/ClickHouseDbRunner.js	(revision Staged)
+++ b/packages/cubejs-schema-compiler/test/integration/clickhouse/ClickHouseDbRunner.js	(date 1692805382899)
@@ -70,7 +70,7 @@
     if (!this.container && !process.env.TEST_CLICKHOUSE_HOST) {
       const version = process.env.TEST_CLICKHOUSE_VERSION || '21.1.2';
 
-      this.container = await new GenericContainer(`yandex/clickhouse-server:${version}`)
+      this.container = await new GenericContainer(`clickhouse/clickhouse-server:latest`)
         .withExposedPorts(8123)
         .start();
     }
```