const CLICKHOUSE_FORMAT = 'JSONEachRow';

const SCHEMA_FILES_LOCATION = 'schema';

module.exports = {
    CLICKHOUSE_FORMAT,
    SCHEMA_FILES_LOCATION,
};