const {
  securityContext: {
    appId
  }
} = COMPILE_CONTEXT;
cube(`TrackedEvents`, {
  sql: `SELECT * FROM ${appId}.snowplow_events`,
  segments: {
    knownUsers: {
      sql: `${CUBE}.user_id_type IN ['cookie', 'identify']`
    }
  },
  measures: {
    pageViewsCount: {
      type: `count`,
      filters: [
        { sql: `${CUBE}.event = 'page_view'` }
      ]
    },
    uniqueUsersCount: {
      type: `countDistinct`,
      sql: `user_id`
    },
    linkClicksCount: {
      type: `count`,
      filters: [
        { sql: `${CUBE}.event_name = 'link_click'` },
      ],
      drillMembers: [ targetUrl, elementId ]
    },
    lastEventTimestamp: {
      sql: `derived_tstamp`,
      type: `max`,
      shown: false
    },
    count: {
      type: `count`,
      drillMembers: [ eventId, pageTitle ]
    }
  },
  dimensions: {
    pageUrlhosts: {
      sql: `page_urlhost`,
      type: `string`
    },
    pageUrlpath: {
      sql: `page_urlpath`,
      type: `string`
    },
    event: {
      sql: `event`,
      type: `string`
    },
    eventId: {
      sql: `event_id`,
      type: `string`,
      primaryKey: true
    },
    eventName: {
      sql: `event_name`,
      type: `string`
    },
    pageTitle: {
      sql: `page_title`,
      type: `string`
    },
    osFamily: {
      sql: `os_family`,
      type: `string`
    },
    osName: {
      sql: `visitParamExtractString(derived_contexts, 'operatingSystemName')`,
      type: `string`
    },
    osVersion: {
      sql: `visitParamExtractString(derived_contexts, 'operatingSystemVersion')`,
      type: `string`
    },
    osVersionMajor: {
      sql: `visitParamExtractString(derived_contexts, 'operatingSystemVersionMajor')`,
      type: `string`
    },
    agentName: {
      sql: `visitParamExtractString(derived_contexts, 'agentName')`,
      type: `string`
    },
    agentVersion: {
      sql: `visitParamExtractString(derived_contexts, 'agentVersion')`,
      type: `string`
    },
    pageReferrer: {
      sql: `page_referrer`,
      type: `string`
    },
    pageUrl: {
      sql: `page_url`,
      type: `string`
    },
    baseUrl: {
      sql: `CONCAT(page_urlscheme, '://', page_urlhost, IF(page_urlport IS NOT NULL AND page_urlport != '', ':', ''), page_urlport)`,
      type: `string`,
    },
    useragent: {
      sql: `useragent`,
      type: `string`
    },
    derivedTstamp: {
      sql: `derived_tstamp`,
      type: `time`
    },
    browserLanguage: {
      sql: `visitParamExtractString(contexts, 'browserLanguage')`,
      type: `string`
    },
    documentLanguage: {
      sql: `visitParamExtractString(contexts, 'documentLanguage')`,
      type: `string`
    },
    viewportSize: {
      sql: `visitParamExtractString(contexts, 'viewport')`,
      type: `string`
    },
    targetUrl: {
      sql: `visitParamExtractString(unstruct_event, 'targetUrl')`,
      type: `string`
    },
    elementId: {
      sql: `visitParamExtractString(unstruct_event, 'elementId')`,
      type: `string`
    },
    customEventName: {
      sql: `custom_event_name`,
      type: `string`
    },
    customEventProps: {
      sql: `custom_event_props`,
      type: `string`
    },
    customUserProps: {
      sql: `custom_user_props`,
      type: `string`
    },
    userId: {
      sql: `user_id`,
      type: `string`
    },
  },
  dataSource: `default`
});
